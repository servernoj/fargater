import { errorHandler } from './'
import { ECSHandler } from '../types'
import get from 'lodash/get'

const listServices: ECSHandler = (ecs, verbose = false) => async context => {
  try {
    const cluster = get(context, 'options.cluster')
    const serviceArns =
      (await ecs
        .listServices({
          cluster,
          launchType: 'FARGATE',
        })
        .promise()
        .then(data => data.serviceArns)
        .catch(errorHandler)) || []

    const services =
      (await ecs
        .describeServices({
          cluster,
          services: serviceArns,
        })
        .promise()
        .then(data => data.services)
        .catch(errorHandler)) || []

    const briefVersion = services.map(({ serviceName }) => serviceName)
    const verboseVersion = services.map(
      ({
        serviceArn,
        clusterArn,
        serviceName,
        status,
        desiredCount,
        runningCount,
        pendingCount,
        taskDefinition,
        // loadBalancers,
        // networkConfiguration,
      }) => ({
        serviceArn,
        clusterArn,
        serviceName,
        status,
        desiredCount,
        runningCount,
        pendingCount,
        taskDefinition,
        // loadBalancers,
        // networkConfiguration,
      }),
    )

    return verbose ? verboseVersion : briefVersion
  } catch (error) {
    throw error
  }
}

export default listServices
