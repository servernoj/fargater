import { errorHandler } from './'
import { ECSHandler } from '../types'
import get from 'lodash/get'
import pick from 'lodash/pick'

const listTasks: ECSHandler = (ecs, verbose = false) => async context => {
  try {
    const cluster = get(context, 'options.cluster')
    const serviceName = get(context, 'options.service')
    const testVerbose = get(context, 'options.verbose')
    const taskArns =
      (await ecs
        .listTasks({
          cluster,
          serviceName,
          launchType: 'FARGATE',
        })
        .promise()
        .then(data => data.taskArns)
        .catch(errorHandler)) || []

    const tasks =
      (await ecs
        .describeTasks({
          cluster,
          tasks: taskArns,
        })
        .promise()
        .then(data => data.tasks)
        .catch(errorHandler)) || []

    const briefVersion = tasks.map(({ taskArn }) => taskArn)
    const verboseVersion = tasks.map(task =>
      pick(task, ['taskArn', 'clusterArn', 'taskDefinitionArn', 'cpu', 'memory', 'attachments']),
    )

    return verbose && testVerbose ? verboseVersion : briefVersion
  } catch (error) {
    throw error
  }
}

export default listTasks
