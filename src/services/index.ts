import { Context, ExecutionHandler } from '../types'
import env from '../env'
import AWS from 'aws-sdk'
import logger from '../logger'
import listClustersWrapper from './list-clusters'
import listServicesWrapper from './list-services'
import listTasksWrapper from './list-tasks'

export const errorHandler = (error: Error) => {
  logger.error(error)
}

const {
  AWS_ACCESS_KEY_ID: accessKeyId,
  AWS_SECRET_ACCESS_KEY: secretAccessKey,
  REGION: region,
} = env

const ecs = new AWS.ECS({
  accessKeyId,
  secretAccessKey,
  region,
  apiVersion: '2014-11-13',
})

export const listClusters = listClustersWrapper(ecs)
export const listServices = listServicesWrapper(ecs)
export const listServicesVerbose = listServicesWrapper(ecs, true)
export const listTasks = listTasksWrapper(ecs)
export const listTasksVerbose = listTasksWrapper(ecs, true)

export const printer = (handler: ExecutionHandler) => async (context: Context) =>
  console.log(JSON.stringify(await handler(context), null, 2))
