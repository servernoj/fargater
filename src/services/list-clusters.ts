import { errorHandler } from './'
import { ECSHandler } from '../types'

const listClusters: ECSHandler = ecs => async () => {
  try {
    const clusterArns =
      (await ecs
        .listClusters()
        .promise()
        .then(data => data.clusterArns)
        .catch(errorHandler)) || []

    const clusters =
      (await ecs
        .describeClusters({ clusters: clusterArns })
        .promise()
        .then(data => data.clusters)
        .catch(errorHandler)) || []

    return clusters.map(({ clusterName }) => clusterName)
  } catch (error) {
    throw error
  }
}

export default listClusters
