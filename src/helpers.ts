import fs, { WriteStream } from 'fs'
import { TabItem, ModuleItem } from './types'

/* Singleton DL allows writing into a file referenced from env DEBUG_LOG */
export const DL = () => {
  let stream: WriteStream
  const formatter = (v: any): string => {
    switch (typeof v) {
      case 'number':
      case 'string':
        return `${v}`
      case 'object':
        return JSON.stringify(v, null, 2)
      default:
        return 'N/A'
    }
  }
  return (data: any, keepItDirty = false) => {
    const { DEBUG_LOG } = process.env
    if (!DEBUG_LOG) {
      return
    }
    if (!stream) {
      stream = fs.createWriteStream(DEBUG_LOG, {
        flags: 'a+',
        encoding: 'utf8',
      })
    }
    if (!keepItDirty) {
      stream.write('\x1b[1J\x1b[1;1H')
    }
    stream.write(formatter(data))
    stream.write('\n')
  }
}

/* Class DDasher (stands for Double Dasher) adds/removes -- in front of */
export class DDasher {
  public static addToItem: (item: ModuleItem) => TabItem = ({ name, description, isCommand }) => ({
    name: isCommand ? name : DDasher.add(name),
    description,
  })
  public static remove: (s: string) => string = s => s.replace(/^-+/, '')
  public static add: (s: string) => string = s => `--${s}`
}
