import tabtab, { TabtabEnv, Json } from 'tabtab'
import minimist from 'minimist3'
import logger from './logger'
import { modulesAsHash, modulesAsArray } from './modules'
import { Context, Effect } from './types'
import { DDasher } from './helpers'
import get from 'lodash/get'

// Name of the CLI utility, defaults to 'fargater' as defined in 'bin' attribute of package.json
const [cliName] = process.argv[1].split('/').slice(-1)

// List of registered CLI params (both commands and options)
const allOptions = modulesAsArray.filter(e => !e.isCommand).map(e => e.name)

// Function called every time when tab-completion is being processed
const completion = async (env: TabtabEnv, context: Context) => {
  // Destructure context to commands and options
  const { commands, options } = context
  // a string param needs a list of possible values to be returned for tab-completion
  if (allOptions.map(DDasher.add).includes(env.prev)) {
    const { handler } = modulesAsHash[DDasher.remove(env.prev)].completion
    const completionProposal = await handler(context)
    tabtab.log(completionProposal.map(e => e.replace(/:/g, '\\:')))
    return
  }
  // filter out all modules with respect to existing context
  const contextKeys = [...Object.keys(options), ...commands]
  const contextEffect = Array.from(
    new Set(
      contextKeys.reduce(
        (effects, key) => {
          const moduleItem = get(modulesAsHash, key)
          const moduleEffects = get(moduleItem, 'completion.effect') || []
          return [...effects, ...moduleEffects]
        },
        [] as Effect[],
      ),
    ),
  )
  const modulesForCompletion = modulesAsArray.filter(moduleItem => {
    const requireOK = moduleItem.completion.require.every(effect => contextEffect.includes(effect))
    const denyOK = !moduleItem.completion.deny.some(effect => contextEffect.includes(effect))
    const notInContext = !contextKeys.includes(moduleItem.name)
    return requireOK && denyOK && notInContext
  })

  return tabtab.log([
    ...modulesForCompletion.map(DDasher.addToItem),
    ...(Object.keys(options).length || commands.length
      ? []
      : [
          {
            name: 'uninstall',
            description: `Uninstall tab-completion feature (can be re-installed by running '${cliName} install')`,
          },
        ]),
  ])
}

const main = async () => {
  // Evaluate the mode in which the app is executed
  const isInCompletionMode = ['completion', '--'].every(e => process.argv.includes(e))
  // Parse _all_ CLI params
  const { _: cliCommands, ...cliOptions } = minimist(
    process.argv.slice(isInCompletionMode ? 4 : 2),
    allOptions,
  )
  const context = modulesAsArray.reduce(
    (ctx, { name, isCommand }) => {
      if (isCommand) {
        if (cliCommands.includes(name)) {
          ctx.commands.push(name)
        }
      } else if (cliOptions[name]) {
        ctx.options[name] = cliOptions[name]
      }
      return ctx
    },
    { commands: [], options: {} } as Context,
  )

  if (cliCommands.some((cmd: string) => /^install$/i.test(cmd))) {
    await tabtab
      .install({
        name: cliName,
        completer: cliName,
      })
      .catch(({ message }) => new Error(logger.inflate('TAB_COMPLETION__INSTALL', { message })))
    return
  }

  if (cliCommands.some((cmd: string) => /^uninstall$/i.test(cmd))) {
    await tabtab
      .uninstall({
        name: cliName,
      })
      .catch(({ message }) => new Error(logger.inflate('TAB_COMPLETION__UNINSTALL', { message })))
    return
  }

  // Handle tab-completion
  if (isInCompletionMode) {
    const { COMP_CWORD, COMP_POINT, COMP_LINE = '' } = process.env
    const env = tabtab.parseEnv({
      COMP_CWORD,
      COMP_POINT,
      COMP_LINE: COMP_LINE.replace(/\\\s+/g, ''),
    } as Json)
    return completion(env, context)
  }

  return context
}

export default main
