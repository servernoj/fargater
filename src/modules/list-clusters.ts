import { listClusters, printer } from '../services'

const item = {
  name: 'list-clusters',
  description: 'List Fargate clusters',
  isCommand: true,
  completion: {
    deny: ['dirty'],
    effect: ['terminal'],
  },
  execution: {
    handler: printer(listClusters),
  },
}

module.exports = item
