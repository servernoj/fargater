import { listTasksVerbose, printer } from '../services'

const item = {
  name: 'list-tasks',
  description: 'List tasks of the selected cluster/service',
  isCommand: true,
  completion: {
    require: ['service is set'],
    effect: ['terminal'],
    deny: ['terminal'],
  },
  execution: {
    handler: printer(listTasksVerbose),
  },
}

module.exports = item
