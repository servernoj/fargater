const item = {
  name: 'verbose',
  description: 'Sets verbose mode for list operations',
  isCommand: false,
  completion: {
    deny: ['dirty'],
  },
}

module.exports = item
