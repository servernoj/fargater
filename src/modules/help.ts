import { Context } from '../types'

const name = 'help'

const handler = ({ options, commands }: Context) => {
  const otherCommands = commands.filter(cmd => !RegExp(name).test(cmd))
  const message =
    Object.keys(options).length || otherCommands.length
      ? `This is a help message in the following CLI context: ${JSON.stringify(
          { options, commands: otherCommands },
          null,
          2,
        )}`
      : `This is a default help message`
  console.log(message)
}

const item = {
  name,
  description: 'Display [context] help',
  isCommand: true,
  completion: {
    effect: ['terminal'],
  },
  execution: {
    priority: 1000,
    handler,
  },
}

module.exports = item
