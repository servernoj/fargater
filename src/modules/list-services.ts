import { listServicesVerbose, printer } from '../services'

const item = {
  name: 'list-services',
  description: 'List services of the selected cluster',
  isCommand: true,
  completion: {
    require: ['cluster is set'],
    effect: ['terminal'],
    deny: ['terminal', 'service is set'],
  },
  execution: {
    handler: printer(listServicesVerbose),
  },
}

module.exports = item
