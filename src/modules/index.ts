import requireDir from 'require-dir'
import { ModuleItem } from '../types'
import deepmerge from 'deepmerge'

const defaultModule = {
  name: 'N/A',
  description: 'N/A',
  isCommand: false,
  completion: {
    handler: () => [],
    effect: ['dirty'],
    require: [],
    deny: [],
  },
  execution: {
    priority: 0,
    handler: console.log,
  },
}

const modulesAsArray = Object.entries(
  requireDir('./', {
    noCache: true,
    extensions: ['.js'],
  } as any),
)
  .filter(([key, value]) => !/^(index)/i.test(key) && !!value)
  .map(([_, value]) => deepmerge(defaultModule, value) as ModuleItem)

const modulesAsHash: Record<string, ModuleItem> = modulesAsArray.reduce(
  (a, i) => ({
    ...a,
    [i.name]: i,
  }),
  {},
)

export { modulesAsHash, modulesAsArray }
