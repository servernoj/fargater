import { listClusters } from '../services'

const item = {
  name: 'cluster',
  description: 'Selects Fargate cluster',
  isCommand: false,
  completion: {
    handler: listClusters,
    deny: ['dirty'],
    effect: ['cluster is set'],
  },
}

module.exports = item
