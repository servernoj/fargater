import { listServices } from '../services'

const item = {
  name: 'service',
  description: 'Selects service of the previously selected cluster',
  isCommand: false,
  completion: {
    handler: listServices,
    require: ['cluster is set'],
    deny: ['terminal'],
    effect: ['service is set'],
  },
}

module.exports = item
