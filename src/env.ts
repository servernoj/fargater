import dotenv from 'dotenv'
import logger from './logger'
import { Context } from './types'
dotenv.config()

const REQUIRED_KEYS = ['AWS_ACCESS_KEY_ID', 'AWS_SECRET_ACCESS_KEY', 'REGION', 'NODE_ENV'] as const
const OPTIONAL_KEYS = [] as const
const ALL_KEYS = [
  ...((REQUIRED_KEYS as unknown) as string[]),
  ...((OPTIONAL_KEYS as unknown) as string[]),
]

export type Environment = {
  [key in typeof REQUIRED_KEYS[number] | typeof OPTIONAL_KEYS[number]]: string
}

const env = Object.freeze(
  Object.entries(process.env)
    .filter(([key, value]) => value && ALL_KEYS.includes(key))
    .reduce((result, [key, value]) => ({ ...result, [key]: value }), {}),
) as Environment

export function validateEnvironment(context: Context | void): Promise<any> {
  if (!context) {
    process.exit(0)
  }
  return new Promise((resolve, reject) => {
    const missingVariables = REQUIRED_KEYS.filter(key => !(key in env))
    if (missingVariables.length) {
      reject(new Error(logger.inflate('APPLICATION__BAD_ENVIRONMENT', { missingVariables })))
    }
    resolve(context)
  })
}

export default env
