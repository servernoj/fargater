import winston, { Logger as WinstonLogger } from 'winston'
import { ErrorMessageParams, ErrorTemplates, ErrorTypes } from './types'

type LoggerFunction = (message: string, info?: unknown) => WinstonLogger
type LoggerFunctionError = (error: Error) => WinstonLogger
type LoggerInterface = {
  [key in 'error' | 'warn' | 'info' | 'verbose' | 'debug']: LoggerFunction | LoggerFunctionError
}

class Logger implements LoggerInterface {
  loggerInstance: WinstonLogger

  constructor() {
    this.loggerInstance = winston.createLogger({
      level: 'debug',
      // format: winston.format.json(),
      exitOnError: false,
      transports: [
        new winston.transports.Console({
          level: 'info',
          format: winston.format.combine(this.infoStringifier(), winston.format.cli()),
        }),
      ],
    })
  }

  infoStringifier = winston.format(info => {
    let details
    try {
      details = JSON.stringify(
        typeof info.info === 'string' ? JSON.parse(info.info) : info.info,
        null,
        2,
      )
    } catch (e) {
      // NOP
    } finally {
      details = details ? `\n${details}` : ''
    }
    info.message = `${info.message}${details}`
    return info
  })

  // Method name 'landr' stands for 'log and return' [error]
  inflate<N extends ErrorTypes>(type: N, params: ErrorMessageParams<N>): string {
    const keys = Object.keys(params as object)
    const values = Object.values(params as object).map(v => {
      switch (typeof v) {
        case 'number':
        case 'string':
          return `${v}`
        case 'object':
          return Array.isArray(v) ? `[${v.join(', ')}]` : JSON.stringify(v, null, 2)
        default:
          return 'N/A'
      }
    })
    // Inflate message template and return the result
    return keys.reduce(
      (a, param, index) => a.replace(new RegExp(`[$]{${param}}`, 'g'), values[index]),
      ErrorTemplates[type],
    )
  }

  flush() {
    this.loggerInstance.end()
  }

  error(data: string | Error, info?: unknown) {
    const message = typeof data === 'string' ? data : data.message
    return this.loggerInstance.log({ level: 'error', message, info })
  }

  warn(message: string, info?: unknown) {
    return this.loggerInstance.log({ level: 'warn', message, info })
  }

  info(message: string, info?: unknown) {
    return this.loggerInstance.log({ level: 'info', message, info })
  }

  verbose(message: string, info?: unknown) {
    return this.loggerInstance.log({ level: 'verbose', message, info })
  }

  debug(message: any, info?: unknown) {
    return this.loggerInstance.log({ level: 'debug', message, info })
  }
}

export default new Logger()
