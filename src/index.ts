#! /usr/bin/env node

import { validateEnvironment } from './env'
import logger from './logger'
import tabCompletion from './tabCompletion'
import action from './action'

tabCompletion()
  .then(validateEnvironment)
  .then(action)
  .catch((error: Error) => {
    logger.error(error)
    logger.flush()
    process.exit(10)
  })
