import { Context, ModuleItem } from './types'
import { modulesAsHash } from './modules'
import get from 'lodash/get'

const [cliName] = process.argv[1].split('/').slice(-1)

const fallbackHandler = () =>
  console.log(`
  Usage: ${cliName} <commands> <options>
`)

export default ({ commands, options }: Context) => {
  const actionModule = commands
    .map(key => modulesAsHash[key])
    .reduce(
      (prev, curr) => {
        return Number(get(curr, 'execution.priority')) >
          Number(get(prev, 'execution.priority') || -1)
          ? curr
          : prev
      },
      undefined as ModuleItem | undefined,
    )
  const handler = get(actionModule, 'execution.handler') || fallbackHandler
  // Execute the handler
  handler({ options, commands })
}
