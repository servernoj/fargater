/* tslint:disable: no-invalid-template-strings */
import { ErrorTypes } from './'

export const ErrorTemplates: { [key in ErrorTypes]: string } = {
  APPLICATION__BAD_ENVIRONMENT: 'Missing environment variables: ${missingVariables}',
  TAB_COMPLETION__INSTALL: 'Tab-completion INSTALL failed: ${message}',
  TAB_COMPLETION__UNINSTALL: 'Tab-completion UNINSTALL failed: ${message}',
}
