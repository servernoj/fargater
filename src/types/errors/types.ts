export type ErrorDefinitions =
  | { type: 'APPLICATION__BAD_ENVIRONMENT'; params: { missingVariables: string[] } }
  | { type: 'TAB_COMPLETION__INSTALL'; params: { message: string } }
  | { type: 'TAB_COMPLETION__UNINSTALL'; params: { message: string[] } }

export type ErrorTypes = ErrorDefinitions extends { type: infer K } ? K : never

export type ErrorMessageParams<K> = Extract<ErrorDefinitions, { type: K }> extends {
  params: infer P
}
  ? P
  : never
