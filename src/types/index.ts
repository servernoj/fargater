export * from './errors'

export type ContextOptions = Record<string, string | boolean>
export type ContextCommands = string[]

export type Context = {
  commands: ContextCommands
  options: ContextOptions
}

export type CompletionHandler = (context: Context) => string[]
export type ExecutionHandler = (context: Context) => any
export type ECSHandler = (ecs: AWS.ECS, verbose?: boolean) => ExecutionHandler | CompletionHandler

export type ModuleItem = {
  name: string
  description: string
  isCommand: boolean
  completion: {
    handler: CompletionHandler
    require: Effect[]
    deny: Effect[]
    effect?: Effect[]
  }
  execution: {
    // The higher the order the greater execution priority
    priority: number
    // The very first handler in execution chain (the one with highest order) recveives context
    // as input parameter, others receive result of the previous handler execution
    handler: ExecutionHandler
  }
}

export type Effect = 'dirty' | 'cluster is set' | 'terminal' | 'service is set'

export type TabItem = { name: string; description: string }
